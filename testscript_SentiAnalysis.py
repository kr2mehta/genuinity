import unittest
import re
import os
#import pytest
#from HTMLTestReportEN import HTMLTestRunner
import HtmlTestRunner
from Genuinity_SentimentAnalysis import SentAnalysis

class testscript_SentiAnalysis(unittest.TestCase):
    
    def test_SentAnalysisNeutralReview(self):
        # Define test inputs
        input_file2 = "test_input.csv"

        # Create test input file for neutral review
        with open(input_file2, "w") as f:
            f.write("title,rating,body\n")
            f.write("Great headset for the price,5.0 out of 5 stars,great headset for the price\n")
    
        # Run SentAnalysis function
        SentAnalysis(input_file2)

        # Check that output file was created
        assert os.path.isfile("outputFile.csv")

        # Check that output file contains correct data
        f = open("outputFile.csv", "r") 
        output1 = f.read()
        f.close()
        status_expected = output1.split("\n")[1].split(",")[-1]
        print(status_expected)
        status_output="neutral"
        message="Test failed"
        self.assertEqual(status_output, status_expected, message)
        
        # Delete test files
        os.remove(input_file2)
        os.remove("outputFile.csv")   

    def test_SentAnalysisNegReview(self):
        #Testing for negative review
        input_file2 = "test_input2.csv"

        # Create test input file
        with open(input_file2, "w") as f:
            f.write("title,rating,body\n")
            f.write("Very chincy,2.0 out of 5 stars,very cheaply made definitely not worth the asking price. thought being from logitech it would be a better value. does not even connect to g hub!\n")

    
        # Run SentAnalysis function
        SentAnalysis(input_file2)
        

        # Check that output file was created
        assert os.path.isfile("outputFile.csv")
        
        # Check that output file contains correct data
        f = open("outputFile.csv", "r") 
        output1 = f.read()
        f.close()
        status_expected = output1.split("\n")[1].split(",")[-1]
        print(status_expected)
        status_output="negative"
        message="Test failed"
        self.assertEqual(status_output, status_expected, message)
        
        # Delete test files
        os.remove(input_file2)
        os.remove("outputFile.csv")   

    def test_SentAnalysisMultipleReviews(self):
        #Testing for multiple review
        input_file2 = "test_input2.csv"

        # Create test input file
        with open(input_file2, "w") as f:
            f.write("title,rating,body\n")
            f.write("Very chincy,2.0 out of 5 stars,very cheaply made definitely not worth the asking price. thought being from logitech it would be a better value. does not even connect to g hub!\n")
            f.write("Great headset for the price,5.0 out of 5 stars,great headset for the price\n")
    
        # Run SentAnalysis function
        SentAnalysis(input_file2)

        # Check that output file was created
        assert os.path.isfile("outputFile.csv")

        # Check that output file contains correct data
        f = open("outputFile.csv", "r") 
        output1 = f.read()
        f.close()
        status1=output1.split("\n")[1].split(",")[-1]
        status2=output1.split("\n")[2].split(",")[-1]
        #print(status1)
        #print(status2)
        expected_status1="negative"
        expected_status2="neutral"
        message="Test failed"
        self.assertEqual(status1, expected_status1, message)
        self.assertEqual(status2, expected_status2, message)
    
        # Delete test files
        os.remove(input_file2)
        os.remove("outputFile.csv")

    def test_SentAnalysisNoComments(self):
        #Testing for multiple review
        input_file2 = "test_input2.csv"

        # Create test input file
        with open(input_file2, "w") as f:
            f.write("title,rating,body\n")
    
        # Run SentAnalysis function
        SentAnalysis(input_file2)

        # Check that output file was created
        assert os.path.isfile("outputFile.csv")

        # Check that output file contains correct data
        f = open("outputFile.csv", "r") 
        output1 = f.read()
        f.close()
        expected_output1 = "No comments" 
        message="Test failed"
        self.assertEqual(output1, expected_output1, message)

        # Delete test files
        os.remove(input_file2)
        os.remove("outputFile.csv")

    def test_SentAnalysisIncorrectRating(self):
        #Testing for multiple review
        input_file2 = "test_input2.csv"

        # Create test input file
        with open(input_file2, "w") as f:
            f.write("title,rating,body\n")
            f.write("Great headset for the price,6.0 out of 5 stars,great headset for the price,0,neutral\n")

        # Run SentAnalysis function
        SentAnalysis(input_file2)
        

        # Check that output file was created
        assert os.path.isfile("outputFile.csv")

        # Check that output file contains correct data
        f = open("outputFile.csv", "r") 
        output1 = f.read()
        f.close()
        message="Test failed"
        expected_output1 = "Incorrect input data" 
        self.assertEqual(output1, expected_output1, message)

        # Delete test files
        os.remove(input_file2)
        os.remove("outputFile.csv")

    def test_SentAnalysisPosReview(self):
        #Testing for positive review
        input_file2 = "test_input2.csv"

        # Create test input file
        with open(input_file2, "w") as f:
            f.write("title,rating,body\n")
            f.write("Works really well,1.0 out of 5 stars,Excellent headphones.\n")

    
        # Run SentAnalysis function
        SentAnalysis(input_file2)
        

        # Check that output file was created
        assert os.path.isfile("outputFile.csv")
        
        # Check that output file contains correct data
        f = open("outputFile.csv", "r") 
        output1 = f.read()
        f.close()
        status_expected = output1.split("\n")[1].split(",")[-1]
        print(status_expected)
        status_output="positive"
        message="Test failed"
        self.assertEqual(status_output, status_expected, message)
        
        # Delete test files
        os.remove(input_file2)
        os.remove("outputFile.csv")      

def Suite():
    
    suiteTest = unittest.TestSuite()

    suiteTest.addTest(testscript_SentiAnalysis("test_SentAnalysisNeutralReview"))
    suiteTest.addTest(testscript_SentiAnalysis("test_SentAnalysisNegReview"))
    suiteTest.addTest(testscript_SentiAnalysis("test_SentAnalysisPosReview"))
    suiteTest.addTest(testscript_SentiAnalysis("test_SentAnalysisMultipleReviews"))
    suiteTest.addTest(testscript_SentiAnalysis("test_SentAnalysisNoComments"))
    suiteTest.addTest(testscript_SentiAnalysis("test_SentAnalysisIncorrectRating"))
    return suiteTest

if __name__ == '__main__':
    # Create HTMLTestRunner report
    #obj = testscript_SentiAnalysis()
    #testscript_SentiAnalysis.test_SentAnalysisMultipleReviews(obj)
    report_file = "test_report.html"
    #print(Suite())
    with open(report_file, "w") as f:
        runner = HtmlTestRunner.HTMLTestRunner(stream=f)
        runner.run(Suite())
    #unittest.main(testRunner=runner, defaultTest='Suite')
        #runner.run(pytest.main(["-v"]))



