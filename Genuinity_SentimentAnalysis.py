import json
import csv
from ibm_watson import NaturalLanguageUnderstandingV1
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from ibm_watson.natural_language_understanding_v1 \
    import Features, SentimentOptions
import Averaging as AS
import requests

url = "http://127.0.0.1:5000/get_data"
data = {"name": "Performing Sentiment Analysis!!!"}

def main(Orating):

    response = requests.post(url, json=data)
    print(response.text)
    authenticator = IAMAuthenticator('Authenticator-Key')
    natural_language_understanding = NaturalLanguageUnderstandingV1(
        version='2022-04-07',
        authenticator=authenticator
    )

    natural_language_understanding.set_service_url('https://api.us-east.natural-language-understanding.watson.cloud.ibm.com/instances/Instance-ID')

    filename = "jsonoutput1.csv"
    
    # initializing the titles and rows list
    fields = []
    rows = []
    scores = []
    # reading csv file
    with open('jsonoutput1.csv', 'r',errors="ignore") as csvfile:
        # creating a csv reader object
        csvreader = csv.reader(csvfile)
        
        # extracting field names through first row
        fields = next(csvreader)
    
        # extracting each data row one by one
        for row in csvreader:
            rows.append(row)
    
    #     # get total number of rows
        print("Total no. of rows: %d"%(csvreader.line_num))
        count = csvreader.line_num
        print("\nThis is Orating : "+Orating)
    
 
    
    for row in rows[:]:
        # parsing each column of a row
        s=""
        for col in row:
            #print(col);
            s=s+" "+col
            
        if len(s) < 20 :
            continue
        response = natural_language_understanding.analyze(text=s, features=Features(sentiment=SentimentOptions())).get_result()

        scores.append(response['sentiment']['document']['score'])
        print("The sentiment label is : "+response['sentiment']['document']['label'])
        print('\n')

    AS.main(Orating,scores,count)

