import requests

url = "http://127.0.0.1:5000/get_data"
data = {"name": "Calculating Normalized rating!!!"}

def main(Orating, scores, count):

    global data
    response = requests.post(url, json=data)
    print("The original rating is  : "+Orating)
    print("\n The scores are : ")
    print(scores)
    print("\n The count is :"+str(count))
    Orating1 = Orating.split(' ')
    norm_rating = (float(Orating1[0]) + sum(scores)/count)
    strr = "Normalized rating is : "+ str(round(norm_rating,3))
    data1 = {"name":strr,"stat":"done"}
    response = requests.post(url, json=data1)
    if norm_rating > 5:
        print("Normalized rating is : " + '5')
    else:
        print("\nNormalized rating is : "+str(norm_rating))