from requests_html import HTMLSession
import json
import time
import requests
import csv
from cleantext import clean
from bs4 import BeautifulSoup
from langdetect import detect
from app import postME
import GenuineReviewFilter
import re


Orating = ""

url = "http://127.0.0.1:5000/get_data"
data = {"name": "Extracting data for the product!!!"}


class Reviews:
    def __init__(self, asin) -> None:
        self.asin = asin
        self.session = HTMLSession()
        self.soup = BeautifulSoup()
        self.headers = {
            'content-type': 'text/html;charset=UTF-8',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'en-US,en;q=0.8',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36 Edg/109.0.1518.61',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        }
        self.url = f'https://www.amazon.com/product-reviews/{self.asin}/ref=cm_cr_arp_d_viewopt_rvwer?ie=UTF8&reviewerType=avp_only_reviews&sortBy=recent&pageNumber='

    def GoingAllPages(self, page,dummy_html):
        if dummy_html is None:
            global Orating
            r = requests.get(self.url+str(page), headers=self.headers)
            self.soup = BeautifulSoup(r.content, 'html.parser')
            if not self.soup.find('div', attrs={'data-hook': 'review'}):
                return False
            else:
                Orating = self.soup.find('span',attrs = {'data-hook':'rating-out-of-text'}).text
                return self.soup.find_all('div', attrs={'data-hook': 'review'})
        else:
            self.soup = BeautifulSoup(dummy_html, 'html.parser')
            if not self.soup.find('div', attrs={'data-hook': 'review'}):
                return False
            else:
                return True

    def parse(self, reviews,flag):
        total = []
        if flag is False:
            for review in reviews:
                title = review.find(
                    'a', attrs={'data-hook': 'review-title'}).span
                rating = review.find(
                    'i', attrs={'data-hook': 'review-star-rating'}).string
                body = review.find(
                    'span', attrs={'data-hook': 'review-body'}).find('span',attrs={'class':''})
                if body:
                    body = body.text
                    data = {'title': title, 'rating': rating, 'body': body}
                    total.append(data)
            print(len(total))
            return total
        
        else:
            for review1 in reviews:
                review = BeautifulSoup(review1, 'html.parser')
                title = review.find(
                    'a', attrs={'data-hook': 'review-title'})
                rating = review.find(
                    'i', attrs={'data-hook': 'review-star-rating'})
                body = review.find(
                    'span', attrs={'data-hook': 'review-body'})
                if body:
                    body = body.text
                    data = {'title': title, 'rating': rating, 'body': body}
                    total.append(data)
                if not body or not title :
                    continue
            if(len(total) != 0):
                return True
            else:
                return False

    def save(self, results):
        # with open(self.asin+'-reviews.json', 'w') as f:
        #     json.dump(results, f)
        data_file = open('jsonoutput.csv', 'w', newline='',encoding="utf-8",errors="ignore")
        csv_writer = csv.writer(data_file)
        count = 0
        for result in results:
            for data in result:
                if count == 0:
                    header = data.keys()
                    csv_writer.writerow(header)
                    count += 1
                csv_writer.writerow(data.values())

        data_file.close()

def Checkid(id):
    if re.match(r"^(?=.{10}$)(?!([A-Z]+|\d+)$)B0[A-Z\d]{8}$",id) :
      return True
    else:
        return False

def main(id):
    if Checkid(id) is True:
        amz = Reviews(id)
        results = []
        response = requests.post(url, json=data)
        print(response.text)
        for x in range(1, 2):
            print('page ', x)
            time.sleep(0.3)
            reviews = amz.GoingAllPages(x,None)
        # print(reviews)
            if reviews is not False:
                results.append(amz.parse(reviews,False))
            else:
                print('no more pages')
                return False
        amz.save(results)
        GenuineReviewFilter.main(Orating)
    


if __name__ == '__main__':

    main(postME)
