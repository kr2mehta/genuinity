# %%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import contractions
import seaborn as sns
from pandas import read_csv
from numpy import set_printoptions
import fasttext
from bs4 import BeautifulSoup
import regex as re
import string
import nltk
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem.wordnet import WordNetLemmatizer
from sklearn.decomposition import PCA
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier 
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.feature_selection import SelectKBest,f_classif
import sklearn.model_selection as model_selection
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.metrics import confusion_matrix,roc_curve,roc_auc_score,cohen_kappa_score,f1_score,recall_score,precision_score,accuracy_score
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder, LabelBinarizer, MultiLabelBinarizer
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer
import sklearn.naive_bayes as naive_bayes 
from sklearn.metrics import classification_report
import sklearn.svm as svm

# %%
amazon_reviews=pd.read_csv('./amazon_reviews.txt', delimiter="\t")
amazon_reviews.loc[amazon_reviews["LABEL"] == "__label1__", "LABEL"] = 'Fake'
amazon_reviews.loc[amazon_reviews["LABEL"] == "__label2__", "LABEL"] = 'Not Fake'
amazon_reviews.head()

# %%
df = amazon_reviews[['LABEL', 'VERIFIED_PURCHASE','REVIEW_TITLE', 'REVIEW_TEXT']]
df.rename(columns = {'LABEL': 'Label', 'VERIFIED_PURCHASE': 'Verified_Purchase', 'REVIEW_TITLE': 'Review_Title', 'REVIEW_TEXT': 'Review_Text'}, inplace = True)

# %%
df['Review_Text'] = df['Review_Title'] + " " + df['Review_Text']
df=df.drop(['Review_Title'], axis=1)
df = df.dropna()

# %%
df = df.dropna()

# %%
df['no_contract'] = df['Review_Text'].apply(lambda x: [contractions.fix(word) for word in x.split()])
df['REVIEW_TEXT_str'] = [' '.join(map(str, l)) for l in df['no_contract']]

# %%
pretrained_model = "./lid.176.bin" 
model = fasttext.load_model(pretrained_model)
langs = []
for sent in df['REVIEW_TEXT_str']:
    lang = model.predict(sent)[0]
    langs.append(str(lang)[11:13])
df['langs'] = langs

# %%
df = df[df['langs'] == 'en']

# %%
def strip_html_tags(text):
    """remove html tags from text"""
    soup = BeautifulSoup(text, "html.parser")
    stripped_text = soup.get_text(separator=" ")
    return stripped_text



new_list = []
for rows in df['REVIEW_TEXT_str']:
    new_list.append(strip_html_tags(rows))
df['tag_removed'] = new_list

# %%
df['number_removed'] = df['tag_removed'].apply(lambda x:re.sub(r'\d+', '', x))

# %%
df['tokenized'] = df['number_removed'].apply(word_tokenize)

# %%
df['lower'] = df['tokenized'].apply(lambda x: [word.lower() for word in x])

# %%
punc = string.punctuation
df['no_punc'] = df['lower'].apply(lambda x: [word for word in x if word not in punc])

# %%
stop_words = set(stopwords.words('english'))
df['stopwords_removed'] = df['no_punc'].apply(lambda x: [word for word in x if word not in stop_words])

# %%
df['pos_tags'] = df['stopwords_removed'].apply(nltk.tag.pos_tag)

def get_wordnet_pos(tag):
    if tag.startswith('J'):
        return wordnet.ADJ
    elif tag.startswith('V'):
        return wordnet.VERB
    elif tag.startswith('N'):
        return wordnet.NOUN
    elif tag.startswith('R'):
        return wordnet.ADV
    else:
        return wordnet.NOUN
    
    
    
    
df['wordnet_pos'] = df['pos_tags'].apply(lambda x: [(word, get_wordnet_pos(pos_tag)) for (word, pos_tag) in x])

# %%
wnl = WordNetLemmatizer()
df['lemmatized'] = df['wordnet_pos'].apply(lambda x: [wnl.lemmatize(word, tag) for word, tag in x])

# %%
df['lemmatized_str'] = [' '.join(map(str, word)) for word in df['lemmatized']]
Train_X, Test_X, Train_Y, Test_Y = model_selection.train_test_split(df['lemmatized_str'],df['Label'],test_size=0.3)

# %%
Tfidf_vect = TfidfVectorizer(max_df=0.80, min_df =50, max_features=5000, use_idf=True)
Tfidf_vect.fit(df['lemmatized_str'])
Train_X_Tfidf = Tfidf_vect.transform(Train_X)
Test_X_Tfidf = Tfidf_vect.transform(Test_X)

# %%
# fit the training dataset on the NB classifier
Naive = naive_bayes.MultinomialNB()
Naive.fit(Train_X_Tfidf,Train_Y)
# predict the labels on validation dataset
predictions_NB = Naive.predict(Test_X_Tfidf)
# Use accuracy_score function to get the accuracy
print("Naive Bayes Accuracy Score -> ",accuracy_score(predictions_NB, Test_Y)*100)
print(confusion_matrix(Test_Y,predictions_NB))
print(classification_report(Test_Y,predictions_NB))

# %%
# Classifier - Algorithm - SVM
# fit the training dataset on the classifier
SVM = svm.SVC(C=1.0, kernel='linear', degree=3, gamma='auto')
SVM.fit(Train_X_Tfidf,Train_Y)
# predict the labels on validation dataset
predictions_SVM = SVM.predict(Test_X_Tfidf)
# Use accuracy_score function to get the accuracy
print("SVM Accuracy Score -> ",accuracy_score(predictions_SVM, Test_Y)*100)
print(confusion_matrix(Test_Y,predictions_SVM))
print(classification_report(Test_Y,predictions_SVM))

# %%
